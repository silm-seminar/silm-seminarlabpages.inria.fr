'''
File: genics.py
Project: silm-seminar
Created Date: Monday May 17th 2021
Author: Ronan (ronan.lashermes@inria.fr)
-----
Last Modified: Thursday, 10th June 2021 2:12:55 pm
Modified By: Ronan (ronan.lashermes@inria.fr>)
-----
Copyright (c) 2021 INRIA
'''

# pip install ics


from os import walk, listdir, makedirs
from os import path
from ics import Calendar, Event, DisplayAlarm
from datetime import timedelta 

destination_folder = "public"
source_folder = "content"

default_location = "Online, on BigBlueButton: https://bbb.irisa.fr/b/las-nqt-ko4-txv with access code 200835"

def get_location(episode_index_path):

    # read file
    md_file = open(episode_index_path,mode='r')
    md_file_content = md_file.read()
    md_file.close()

    # split metadata and data
    split_file = md_file_content.split("+++")
    if len(split_file) < 2:
        return default_location
        
    metadata = split_file[1]
    meta_statements = metadata.splitlines()

    for ms in meta_statements:
        if "=" in ms:
            split_statement = ms.split("=")
            var_name = split_statement[0].strip()
            var_val  = split_statement[1].strip().replace('"', '')

            if var_name == "location":
                print(var_val)
                return var_val

    return default_location

def slugify(instr):
    instr = instr.lower()
    instr = instr.replace(".", "")
    instr = instr.replace(" ", "-")
    instr = instr.replace("é", "e")
    instr = instr.replace("è", "e")
    instr = instr.replace("ê", "e")
    return instr

def generate_event(md_path, location):
    # print(md_path)
    # e = Event(alarms = [DisplayAlarm(timedelta(minutes=-5))])
    e = Event()

    # read file
    md_file = open(md_path,mode='r')
    md_file_content = md_file.read()
    md_file.close()

    # split metadata and abstract
    split_file = md_file_content.split("+++")
    if len(split_file) != 3:
        return e
        
    metadata = split_file[1]
    talk_abstract = split_file[2]

    talk_title = ""
    talk_date = ""
    talk_speaker = ""
    talk_speaker_institution = ""
    talk_duration = "60"
    
    # parse metadata
    meta_statements = metadata.splitlines()
    for ms in meta_statements:
        if "=" in ms:
            split_statement = ms.split("=")
            var_name = split_statement[0].strip()
            var_val  = split_statement[1].strip().replace('"', '')

            if var_name == "title":
                talk_title = var_val
            elif var_name == "date":
                talk_date = var_val
            elif var_name == "speaker_name":
                talk_speaker = var_val
            elif var_name == "speaker_institution":
                talk_speaker_institution = var_val
            elif var_name == "duration":
                talk_duration = var_val


    e.name = talk_title + " - " + talk_speaker + " (" + talk_speaker_institution + ")"
    # print(e.name)
    e.location = location
    e.begin = talk_date
    e.duration = timedelta(minutes=int(talk_duration))
    e.description = talk_abstract
    e.uid = slugify(talk_speaker)

    return e


# one calendar per season
for season_name in listdir(source_folder):
    if season_name.startswith("season") == False:
        continue

    cal = Calendar()
    current_folder = source_folder + "/" + season_name
    
    # look for all .md files in the season folder
    talk_paths = []
    for (dirpath, dirnames, filenames) in walk(current_folder):
        location = get_location(dirpath + "/" + "_index.md")
        for file_path in filenames:
            filename, file_extension = path.splitext(file_path)
            if file_extension == ".md" and filename != "_index":
                e = generate_event(dirpath + "/" + file_path, location)
                cal.events.add(e)
    

    dest_file_name = destination_folder + "/" + season_name + '/all_talks.ics'
    if not path.exists(path.dirname(dest_file_name)):
        makedirs(path.dirname(dest_file_name))
    with open(dest_file_name, 'w') as f:
        f.writelines(cal)