+++
title = "Micro-Evolving Monolithic Systems with the Nested Kernel Architecture"
date = 2021-05-21T13:00:00Z

[extra]
speaker_name = "Nathan Dautenhanh"
speaker_institution = "Rice University"
links = ["memorizer", "nested"]
inria_video_link = "https://videos-rennes.inria.fr/video/0FEM-s6i_"

memorizer_link = "https://memorizer.tk/"
memorizer_label = "Memorizer"

nested_link = "http://nestedkernel.github.io/"
nested_label = "Nested kernel"
+++
Information protection has been a challenge since the inception of computing, where modern environments prioritize functionality over security, resulting in *permiscuous* monolithic runtimes that allow access to more information than necessary. As a result, we continue to see attacks from the low-level (e.g. control-flow hijack) to the high-level (e.g. privilege escalation, information leakage, confused deputies). Least-authority compartmentalization is the standard solution, however, most techniques are single purpose, hard to use, and inefficient.

In this talk, I describe the Nested Kernel architecture, a general purpose operating system organization and methodology that *nests* an efficient, tamper-proof security monitor directly into monolithic runtimes. The Nested Kernel is then used to provide data protection and separation services for decomposing and securing elements *inside* the system. One of the core insights is to virtualize privilege using self-protection techniques.  Similar to but much smaller than a microkernel, a nested kernel provides isolation within single-address space environments for the sake of performance, ease of integration, and programmable protection.  Although similar to an Exokernel, the Nested Kernel is a specialized kernel that limits access by virtualizing a thin interface so that protection can be enforced on that runtime itself, rather than safely exporting its use. 

The Nested Kernel has been implemented in many prototypes, demonstrating efficient and portable protection in diverse system software (Xen, FreeBSD, Linux, Android, UNIX Processes) while using commodity and custom hardware (ARM-PT, ARM-TZ, ARM-NPT, x86-64, VT-x, SFI, VMFUNC, and MPK), and even influenced the design of custom Apple hardware. Nested Kernel prototypes demonstrate that it is possible to retrofit security into existing and popular systems with programmable, powerful, and incrementally deployable intra-space protection services, providing the foundation for future work in securing our systems.  I conclude by sketching a path forward for a "micro-evolution" of monolithic systems.