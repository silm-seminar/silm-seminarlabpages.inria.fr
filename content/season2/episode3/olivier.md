+++
title = "Exploring Multi-ISA and Emerging Isolation Technologies for Systems Software Security"
date = 2021-05-21T12:00:00Z

[extra]
speaker_name = "Pierre Olivier"
speaker_institution = "University of Manchester"

links = ["hetersec_web", "hetersec_code", "hermitmpk_web", "hermitmpk_code"]
inria_video_link = "https://videos-rennes.inria.fr/video/JU7ArQPJy"

hetersec_web_link = "http://www.popcornlinux.org/index.php/hetersec"
hetersec_web_label = "HeterSec website (with papers)"
hetersec_code_link = "https://github.com/ssrg-vt/HeterSec"
hetersec_code_label = "HeterSec's github"
hetersec_code_type = "fab fa-github"

hermitmpk_web_link = "https://ssrg-vt.github.io/libhermitMPK/"
hermitmpk_web_label = "LibHermitMPK website (with paper)"
hermitmpk_code_link = "https://github.com/ssrg-vt/libhermitMPK"
hermitmpk_code_label = "LibHermitMPK's github"
hermitmpk_code_type = "fab fa-github"

+++
Systems software such as Operating Systems (OS) or hypervisors are a cornerstone of modern computer systems' security: they aim to protect and isolate user applications, while being themselves resilient against various types of attacks. In this talk I will present a set of contributions exploring the use of multi-ISA systems as well as emerging isolation technologies for systems software security.
In a first part of the talk, I will present HeterSec, a framework allowing to secure applications by executing them on top of multi-ISA systems composed of commodity servers.
Next, I will present LibHermitMPK, a unikernel (library OS) written in Rust and leveraging the Memory Protection Keys (MPK) technology to provide isolation between user and kernel space, as well as between safe and unsafe kernel components.
Finally, I will talk about ongoing work regarding the development of FlexOS, a flexible and modular OS design whose compartmentalization and protection profile can easily and cost-efficiently be tailored towards a specific application or use-case at build time, rather than at design time as it is the case for traditional OSes.