+++
title = "Episode 2"
sort_by = "date"
template = "episode.html"
page_template = "speaker.html"
weight = 2

[extra]
live = false
date = 2021-04-23T12:00:00Z
+++