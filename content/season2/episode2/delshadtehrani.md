+++
title = "Improving security through a programmable hardware monitor"
date = 2021-04-23T13:00:00Z

[extra]
speaker_name = "Leila Delshadtehrani"
speaker_institution = "Boston University"
links = ["phmon_paper_web", "phmon_paper_pdf", "phmon_github"]
inria_video_link = "https://videos-rennes.inria.fr/video/xhTxCRoGd"

phmon_paper_web_link = "https://link.springer.com/chapter/10.1007/978-3-030-52683-2_13"
phmon_paper_web_label = "Efficient Context-Sensitive CFI Enforcement Through a Hardware Monitor, DIMVA'20"

phmon_paper_pdf_link = "https://link.springer.com/content/pdf/10.1007%2F978-3-030-52683-2_13.pdf"
phmon_paper_pdf_label = "Efficient Context-Sensitive CFI Enforcement Through a Hardware Monitor, DIMVA'20"
phmon_paper_pdf_type = "fas fa-file-pdf"

phmon_github_link = "https://github.com/bu-icsg/PHMon"
phmon_github_label = "PHMon's github repository"
phmon_github_type = "fab fa-github"
+++
In recent years, security breaches of computing systems have become very common; hence, security has become a first-class design requirement. Recently, there has been a growing trend in the industry for hardware-assisted security features. 
The current trend implements dedicated hardware security extensions through an imperfect, lengthy, and costly process. These dedicated hardware extensions built in silicon cannot get updated at the same pace as security threats evolve. In contrast, a flexible hardware monitor can efficiently enforce and enhance a variety of security policies as security threats evolve. 
In this talk, I will present our work on a minimally invasive and efficient implementation of a Programmable Hardware Monitor (PHMon) with the full software stack around it. PHMon can enforce a variety of security policies and can also assist with detecting software bugs and security vulnerabilities. To demonstrate PHMon’s versatility and its ease of adoption, I will discuss some of PHMon’s use cases.