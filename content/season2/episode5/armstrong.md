+++
title = "Isla: Axiomatic Concurrency Semantics for ARMv8-A and RISC-V using Sail"
date = 2021-06-11T12:00:00Z

[extra]
speaker_name = "Alasdair Armstrong"
speaker_institution = "University of Cambridge"
links = ["isla_web", "sail_paper", "fetchsem_paper"]
inria_video_link = "https://videos-rennes.inria.fr/video/hDQ9aVct6"

isla_web_link = "https://isla-axiomatic.cl.cam.ac.uk/"
isla_web_label = "Isla online interface"

sail_paper_link = "https://dl.acm.org/doi/10.1145/3290384"
sail_paper_label = "Sail POPL19 paper"

fetchsem_paper_link = "https://link.springer.com/chapter/10.1007%2F978-3-030-44914-8_23"
fetchsem_paper_label = "Instruction fetch semantics ESOP20 paper"
+++
Architecture specifications such as ARMv8-A and RISC-V are the ultimate foundation for software verification and the correctness criteria for hardware verification. They should define the allowed sequential and relaxed-memory concurrency behaviour of programs, but hitherto there has been no integration of full-scale instruction-set architecture (ISA) semantics with axiomatic concurrency models, these ISA semantics can be surprisingly large and intricate, e.g. 100k+lines for ARMv8-A.

Sail is a custom domain-specific language for ISA semantics, in which we have developed formal models for ARMv8-A, RISC-V, and MIPS. In this talk I will discuss our work on formalising these architectures, and present a tool Isla, which is capable of simulating relaxed memory models over these large architecture specifications using symbolic execution. This allows us to consider aspects of the concurrent behavior of these architectures not previously considered by other tools, but which are relevant to systems software such as operating systems and hypervisors. These aspects include feature such as instruction fetch behavior and address translation. 