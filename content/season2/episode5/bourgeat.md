+++
title = "Rule-based hardware design languages and formal methods"
date = 2021-06-11T13:00:00Z

[extra]
speaker_name = "Thomas Bourgeat"
speaker_institution = "MIT CSAIL"
links = ["koika", "bluespec", "asplos"]
inria_video_link = "https://videos-rennes.inria.fr/video/0z0P7GIo2"

koika_link = "https://github.com/mit-plv/koika"
koika_label = "Koika git's repository"
koika_type = "fab fa-github"

bluespec_link = "https://dl.acm.org/doi/10.1145/3385412.3385965"
bluespec_label = "Bluespec PLDI20 paper"

asplos_link = "https://dl.acm.org/doi/10.1145/3445814.3446720"
asplos_label = "ASPLOS21 paper on rule-based hardware-design languages"
+++
In this talk, we will give an introduction to rule-based programming and its suitability for formal reasoning of hardware design. We will specifically present the semantics of the Koika language, an EDSL in Coq, that allows precise reasoning about the functional and performance (cycle count) properties of hardware design.

