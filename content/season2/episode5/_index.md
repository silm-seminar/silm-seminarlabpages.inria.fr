+++
title = "Episode 5"
sort_by = "date"
template = "episode.html"
page_template = "speaker.html"
weight = 5

[extra]
live = false
date = 2021-06-11T12:00:00Z
location="On livestorm, please register on https://app.livestorm.co/inria-14/seminaire-silm?type=detailed"
+++