+++
title = "Episode 6"
sort_by = "date"
template = "episode.html"
page_template = "speaker.html"
weight = 6

[extra]
live = false
date = 2021-06-18T14:00:00Z
+++