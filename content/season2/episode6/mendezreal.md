+++
title = "Security vulnerabilities of energy management systems"
date = 2021-06-18T14:00:00Z

[extra]
speaker_name = "Maria Mendez Real"
speaker_institution = "Université de Nantes"

+++
Remote attacks exploiting energy management systems from software have recently emerged showing that these latter are vulnerable and can be attacked leading to fault injection, for confidentiality and denial of service attacks.
In this talk we present recent work on the implementation on remote attacks targeting Dynamic Voltage and Frequency Scaling and their capabilities on multi-core systems. Finally, we introduce ongoing work on the investigation of exploiting embedded sensors measurements, on which energy managers rely to take runtime decisions, in order to gain information on manipulated data and executed instructions that will otherwise be non-accessible. The proposed methodology has been successfully applied in order to recover AES encryption key.