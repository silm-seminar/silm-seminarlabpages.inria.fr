+++
title = "Processeur intrinsèquement sécurisé par le chiffrement authentifié des données et des instructions"
date = 2021-03-29T12:00:00Z

[extra]
speaker_name = "Olivier Savry"
speaker_institution = "CEA-Leti"
inria_video_link = "https://videos-rennes.inria.fr/video/Yfk_FL6hI"
+++
Suite à un état de l’art exhaustif des vulnérabilités des processeurs, nous avons fait le constat que les failles étaient souvent dues à une méconnaissances des règles de développement de codes sécurisés et à des mauvaises conceptions de la microarchitecture. Nous avons alors essayé d’avoir une résolution globale de ces problèmes en proposant le concept de processeurs d’applications intrinsèquement sécurisés qui soulagerait le développeur d’avoir à prendre en compte ces faiblesses.
Notre solution se base sur le chiffrement authentifié des instructions et des données qui a fait l’objet d’une preuve de concept sur un processeur RISC V (RISCY 32 bits) sur FPGA. Malgré un surcoût important qui sera davantage dilué dans un processeur d’applications, nous avons montré qu’il permet d’atteindre de nombreux objectifs de sécurité : confidentialité, intégrité et authenticité du code au démarrage mais surtout à l’exécution. Egalement il permet de résoudre la plupart des vulnérabilités des programmes.
Nous montrerons dans cet exposé comment nous avons pu implémenter un CFI en rajoutant quelques instructions et en modifiant une toolchain LLVM. Nous avons aussi adressé le sempiternel problème des fuites spatiales et temporelles de mémoire (buffer overflows en tout genre, dangling pointers, use after free, …). Cette contremesure utilise les bits inutilisés dans les adresses 64 bits pour y insérer des identifiants de pointeurs nécessaires au chiffrement. Cela permet d’assurer que des débordements sur des données ne soient pas exploitables.
Enfin, indépendamment du chiffrement nous avons également développé un cache avec un adressage aléatoire permettant d’éviter les fuites d’adresses dans les attaques de type cache timing.