+++
title = "Protecting Commodity Software against Data-only Attacks using Hardware-assisted Virtualization"
date = 2021-05-28T13:00:00Z

[extra]
speaker_name = "Vasileios P. Kemerlis"
speaker_institution = "Brown University"


+++
Contemporary software systems consist of large, monolithic blobs of complex code, and are plagued with vulnerabilities that allow perpetrators to exploit them for profit. Modern adversaries typically leverage memory corruption bugs to establish primitives for reading-from, or writing-to, the address space of vulnerable processes. Such primitives form the foundation for code-reuse and data-oriented attacks, and while various defenses against the former class of attacks have proven somewhat effective, mitigations against the latter remain an open problem.

In this talk, I will discuss how we can leverage (HW-assisted) virtualization extensions to build effective defenses against data-oriented attacks. In particular, I will present xMP: a recent (IEEE S&P 2020) project of ours whose goal is to provide selective memory protection primitives for isolating sensitive data, both in user and kernel space, in disjoint security domains. Contrary to conventional approaches, xMP takes advantage of virtualization extensions, and, after initialization, it does not require any hypervisor intervention.

More importantly, to ensure the integrity of in-kernel management information, and pointers to sensitive data within isolated domains, xMP protects security-critical pointers with HMACs, bound to an immutable context, so that integrity validation succeeds only in the right context. We have applied xMP to protect the page tables and process credentials of the Linux kernel, as well as sensitive data in various user-space applications. Overall, our evaluation shows that xMP introduces minimal overhead for real-world workloads and applications, and offers effective protection against data-oriented attacks.
