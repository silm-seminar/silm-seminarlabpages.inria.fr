+++
title = "Radically secure computing"
date = 2020-02-18T08:45:00Z

[extra]
speaker_name = "Ronan Lashermes"
speaker_institution = "Inria/SED&LHS"
inria_video_link = "https://videos-rennes.inria.fr/video/ryQJPpihU"
duration = 30
+++
Past experiences have shown that ensuring security in any computing system is a difficult task. 
When software security is starting to mature, micro-architectural attacks sneak in! And physical attacks are not far.
Instead of blaming people for bad implementations, we will argue for a new radical solution to ensure secure computing: the instruction set architecture should ensure strong formal security guarantees by restricting the computational model.
