+++
title = "Physical side-channel analysis on STM32F{0,1,2,3,4}"
date = 2020-02-18T13:30:00Z

[extra]
speaker_name = "Annelie Heuser"
speaker_institution = "CRNS/TAMIS"
inria_video_link = "https://videos-rennes.inria.fr/video/Hyq_nDR7D"
duration = 30
+++
Physical side-channel attacks use unintentionally omitted information of physical devices to predict internally processed data. 
In this talk, we will present attacks using power consumption and electromagnetic emanations captured on STM32F0 to F4.
Interestingly, all devices present similar and yet different behaviours in terms of side-channel vulnerabilities.