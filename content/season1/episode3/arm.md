+++
title = "Fruitful security (and more) from CHERI to Morello"
date = 2020-02-18T10:00:00Z

[extra]
speaker_name = "Arnaud de Grandmaison"
speaker_institution = "Arm"
inria_video_link = "https://videos-rennes.inria.fr/video/HJrTY_RQw"
+++
Security is the greatest challenge computing needs to address to meet its full potential. 
Memory safety issues have been for far too long and way too common in the field, with the first partially documented one dating from 1972 ! 
Almost 50 years later, it’s obvious the situation has only gotten worse, with for example Microsoft (and this is not specific to them) recognizing that 70% of all security bugs in their products are memory safety issues.
Capability-based (software managed but hardware enforced tokens of authority used to access memory) systems have been researched and used since the very beginning of computing, but they have been set aside by segmentation/pagination-based systems for price, performance and ease of implementation reasons. 
Arm has been working with the University of Cambridge for several years to come up with Morello, an Arm implementation of the CHERI architecture, to address security with strong fundamental principles. 
This is a big and critical project, as recognized by the large funding from the UK government (£70 million), and the £117 million additional contribution from the “Digital Security by Design” partners.
This presentation will first introduce capability-based systems in general, and CHERI specifically then focus on Morello. 
Transitioning to capabilities is challenging and disruptive enough, at all levels from the hardware up to the software, system, debug, … that this is the right time to look at those (not so) new systems again, hopefully benefiting from 50+ years of experience, if not errors, in security. 
This is effectively a call to arms (pun intended !) for a joint research – industry collaboration effort.
