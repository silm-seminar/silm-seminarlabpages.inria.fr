+++
title = "HardBlare, a hardware/software co-design approach for Information Flow Control"
date = 2020-02-18T13:00:00Z

[extra]
speaker_name = "Guillaume Hiet"
speaker_institution = "CentraleSupélec/CIDRE"
duration = 30
+++
HardBlare proposes a hardware/software solution to implement an anomaly-based approach to detect software attacks against confidentiality and integrity through Dynamic Information Flow Tracking (DIFT). 
HardBlare combines fine-grained DIFT with OS-level tagging which allows end-user to specify security policies. 
Main outcome at the hardware level of HardBlare is the design of a dedicated multi-core DIFT co-processor on FPGA that does not require any modification of the main CPU. 
This contribution tackles one of the main challenges of the project. 
To achieve this goal, information required for DIFT is obtained both at compile-time and run-time through pre-computation during the compilation step, hardware trace mechanisms and instrumentation at run-time. 
Main outcome at the software level of HardBlare is the Linux kernel modification to handle file tags and to support communication between the instrumented code and the co-processor, a patch of the official Linux PTM driver, Linux loader (ld.so) modification to load annotations to the co-processor and a LLVM backend pass to compute the annotations and instrument applications. 
All validation has been performed on the Digilent ZedBoard using Xilinx ZYNQ SoC which combines two hardcores (ARM Cortex-A9) with a Xilinx FPGA. 
Through its achievements HardBlare demonstrates that a hardware/software co-design approach for Information Flow Control corresponds to an efficient and promising solution.
