+++
title = "Port Contention for Fun and Profit"
date = 2020-01-17T09:00:00Z

[extra]
speaker_name = "Billy Brumley"
speaker_institution = "Tampere University of Technology"
inria_video_link = "https://videos-rennes.inria.fr/video/BkcYRuRNU"
+++
Simultaneous Multithreading (SMT) architectures are attractive targets for attackers with side-channel expertise. SMT inherently offers a broader attack surface, exposing more microarchitecture components per physical core for fine-grain attacks. PortSmash (CVE-2018-5407) is a technique that abuses the execution units to exploit port contention, and creates a high-resolution timing side-channel capable of leaking confidential information. PortSmash affects both Intel and AMD architectures featuring SMT technology and due to its nature, it is capable of targetting shared libraries, static builds and even SGX enclaves.