+++
title = "Episode 2"
sort_by = "date"
template = "episode.html"
page_template = "speaker.html"
weight = 2

[extra]
live = false
date = 2020-01-17T09:00:00Z
+++