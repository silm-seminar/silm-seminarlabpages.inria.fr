+++
title = "Episode 1"
sort_by = "date"
template = "episode.html"
page_template = "speaker.html"
weight = 1

[extra]
live = false
date = 2019-11-08T09:00:00Z
+++